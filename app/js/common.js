// $(window).on('load', function () {
//     $('#preload').addClass('no-active-preload')
// });
setTimeout(function () {
    $('#preload').addClass('no-active-preload');
}, 1500);
$(document).ready(function() {

	// Custom JS

    var wow  =  new  WOW(
        {
            mobile: false
        }
    );
    wow.init();
    if($('#counts').length){
        var show = true;
        var countbox = "#counts";
        $(window).on("scroll load resize", function(){
            if(!show) return false;                   // Отменяем показ анимации, если она уже была выполнена
            var w_top = $(window).scrollTop();        // Количество пикселей на которое была прокручена страница
            var e_top = $(countbox).offset().top;     // Расстояние от блока со счетчиками до верха всего документа
            var w_height = $(window).height();        // Высота окна браузера
            var d_height = $(document).height();      // Высота всего документа
            var e_height = $(countbox).outerHeight(); // Полная высота блока со счетчиками
            if(w_top + 400 >= e_top || w_height + w_top == d_height || e_height + e_top < w_height){
                $(".spincrement").spincrement({
                    from: 0,
                    thousandSeparator: "",
                    duration: 3000
                });
                show = false;
            }
        });
    }


    var media1020 = window.matchMedia('all and (max-width: 1020px)');
    if($('.home-about-us').length){
        var aboutUsTop = $('.home-about-us').offset().top;
    }
    if($('.form-block').length){
        var formBlockTop = $('.form-block').offset().top;
    }

    $('#strela').on('click touchdown', function () {
        $('html, body').animate({ scrollTop: 750 }, 500);
        return false;
    });
    // $('#callback').on('click touchdown', function () {
    //     $('html, body').animate({ scrollTop: formBlockTop }, 2000);
    //     return false;
    // });

    // Проверка формы на пустоту
    $('#contact-form').submit(function () {
        flag = true;
        in_name = $(this).find('.in_name').val();
        in_phone = $(this).find('.in_phone').val();
        in_email = $(this).find('.in_email').val();

        if(in_name == ''){
            $(this).find('.in_name').addClass('form-error');
            flag = false;
        }
        if(in_phone == ''){
            $(this).find('.in_phone').addClass('form-error');
            flag = false;
        }
        if(in_email == ''){
            $(this).find('.in_email').addClass('form-error');
            flag = false;
        }
        if(flag){
            $.fancybox.open({
                src: '#thank-modal',
                type: 'inline'
            });
        }
        return false;
    });

    $('#contact-form').find('input').focus(function () {
        $(this).removeClass('form-error');
    });

    // Слайдер главной
    $('#slider-top').slick({
        fade: true,
        dots: true,
        arrows: true,
        nextArrow: '<span class="slick-next"><img src="img/arrow-rite.svg"></span>',
        prevArrow: '<span class="slick-prev"><img src="img/arrow-left.svg"></i></span>',
        autoplay: true,
        autoplaySpeed: 10000,
        imfinity: true,
        speed: 1000,
        cssEase: 'linear',
        slidesToShow: 1,
        slidesToScroll: 1
    });
    var slickk=$('.home-slider');
    var slickCount = slickk.slick("getSlick").slideCount;
    if(slickCount >= 10 ){
        $('#count-slide').html(slickCount);
    }else{
        $('#count-slide').html("0"+slickCount);
    }

    $('.home-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        var indexSlide = nextSlide + 1;
        if(indexSlide >= 10 ){
            $('#index-slide').html(indexSlide);
        }else{
            $('#index-slide').html("0"+indexSlide);
        }
    });


    // Слайдер Наша команда
    $('.comand-slider-bottom').slick({
        dots: false,
        arrows: true,
        nextArrow: '<span class="slick-next"><img src="img/arrow-rite-black.svg"></span>',
        prevArrow: '<span class="slick-prev"><img src="img/arrow-left-black.svg"></i></span>',
        imfinity: true,
        slidesToShow: 5,
        slidesToScroll: 1
    });

    $('.comand-smal-block').on('click touchdown', function (e) {
        e.preventDefault();
       id = $(this).data('slide');
       $('.comand-big-block').removeClass('active');
       $('#'+id).addClass('active');
    });
    var slider_init = false;

    if($(window).width() <= 992){
        slider_init = true;
        $('.comand-slider-top').slick({
            dots: false,
            arrows: true,
            nextArrow: '<span class="slick-next"><img src="img/arrow-rite-black.svg"></span>',
            prevArrow: '<span class="slick-prev"><img src="img/arrow-left-black.svg"></i></span>',
            imfinity: true,
            slidesToShow: 1,
            slidesToScroll: 1
        });
    }
        if(slider_init){
            $(window).resize(function() {
                if($(window).width() <= 992){
                    $('.comand-slider-top').slick('refresh');
                }else{
                    $('.comand-slider-top').slick('unslick');
                }
            });
        }

     // слайдер одного проекта
    $('#project_slider').slick({
        dots: false,
        arrows: true,
        nextArrow: '<span class="slick-next"><img src="img/arrow-rite-black.svg"></span>',
        prevArrow: '<span class="slick-prev"><img src="img/arrow-left-black.svg"></i></span>',
        imfinity: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 630,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });




    // появление табов
    $('.tabs-top').on('click touchdown', function (e) {
        e.preventDefault();
        if($('.tabs-ul-mobile').is(':visible')){
            $('.tabs-ul-mobile').hide('fast');
            $(this).children('.arrow').addClass('rotate90');
        }else{
            $('.tabs-ul-mobile').show('fast');
            $(this).children('.arrow').removeClass('rotate90');
        }

    });
    $('.home-project .tabs-ul-mobile li a').on('click touchdown', function () {
        if($(this).parent('li').hasClass('active')){
            return false;
        }else{
            project = $(this).data('project');
            if( project == 'project-col'){
                $('.tabs-top .title').text('Все проекты')
            }
            if( project == 'project-completed'){
                $('.tabs-top .title').text('Завершенные')
            }
            if( project == 'project-during'){
                $('.tabs-top .title').text('В процессе')
            }
            $('.tabs-ul-mobile li').removeClass('active');
            $('.project-col').hide();
            $('.'+project).show();
            $(this).parent('li').parent('.tabs-ul-mobile').hide();
            $(this).parent('li').addClass('active');
        }
        return false;
    });
    $('.home-project .tabs-ul-desktop li a').on('click touchdown', function () {
        if($(this).parent('li').hasClass('active')){
            return false;
        }else{
            project = $(this).data('project');
            $('.tabs-ul-desktop li').removeClass('active');
            $('.project-col').hide();
            $('.'+project).show();
            $(this).parent('li').addClass('active');
        }
        return false;
    });

    $('.category-block .title').on('click touchdown', function () {
        if($(this).parent('.category-block').find('.dropdown-ul-first').hasClass('active')){
            $(this).parent('.category-block').find('.dropdown-ul-first').hide('fast').removeClass('active');
            $(this).children('i').addClass('rotate90');
        }else{
            $(this).parent('.category-block').find('.dropdown-ul-first').show('fast').addClass('active');
            $(this).children('i').removeClass('rotate90');
        }
        return false;
    });

    $('.dropdown-ul-first li a').on('click touchdown', function () {
        if($(this).parent('li').find('.dropdown-ul').hasClass('active')){
            $(this).parent('li').find('.dropdown-ul').hide('fast').removeClass('active');
            $(this).parent('li').removeClass('active');
            $(this).children('i').addClass('rotate90');
        }else{
            $(this).parent('li').find('.dropdown-ul').show('fast').addClass('active');
            $(this).parent('li').addClass('active');
            $(this).children('i').removeClass('rotate90');
        }
        return false;
    });


    // Главное меню
    $('#menu-open').on('click touchdown', function () {
        $('.main-menu').show('fast');
        return false;
    });
    $('#menu-close').on('click touchdown', function () {
        $('.main-menu').hide('fast');
        return false;
    });

    // Изминение языка

        $('.mob-lang-box li.active ').on('click touchdown', function () {
            $('.lang-mobile-block').show('fast');
            return false;
        });
        $('#lang-close').on('click touchdown', function () {
            $('.lang-mobile-block').hide('fast');
            return false;
        });

    // Блоки проектов
    $('.all-project-wrapper').masonry({
        itemSelector: '.project-masonry',
        gutter: '.gutter-project',
        percentPosition: true
    });
    // картинки одного проекта
    if($(window).width() >760){
        $('.slider-img-project').on('click touchdown', function (e) {
            e.preventDefault();
            index = $(this).data('index');
            img = $(this).children('img').attr('src');
            $('#big-img-project').addClass('img-anime').attr('src', img).parent('.big-img-block').data('index', index);
            setTimeout(function () {
                $('#big-img-project').removeClass('img-anime');
            }, 500)
        });
    }else{
        $('[data-fancybox="images"]:not(".slick-cloned")').fancybox({
                idleTime  : false,
                margin    : 0,
                gutter    : 0,
                infobar   : false,
                loop: true,
                zoom: false,
                mobile : {
                    idleTime : false,
                    margin   : 0,

                }
            });
    }

    $('#click-big-img-project').on('click touchdown', function (e) {
        e.preventDefault();
        index = $(this).data('index');
        $.fancybox.open($('[data-fancybox="images"]:not(".slick-cloned")'),{
            idleTime  : false,
            margin    : 0,
            gutter    : 0,
            infobar   : true,
            loop: true
        },index);
    });


    //Одинаковая высота
    $('.achievement-block').equalHeightResponsive();
    $('.services-block').equalHeightResponsive();
    $('.project-col').equalHeightResponsive();
    $('.blog-block').equalHeightResponsive();
    $(window).resize(function() {
        setTimeout(function () {
            $('.achievement-block').equalHeightResponsive('refresh');
            $('.services-block').equalHeightResponsive('refresh');
            $('.project-col').equalHeightResponsive('refresh');
            $('.blog-block').equalHeightResponsive('refresh');
        },100);
    });


});
