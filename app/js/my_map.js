var map, marker;
var icon = 'img/gps.svg';

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 55.781463, lng: 37.520376},
        zoom: 12
    });
    marker = new google.maps.Marker({
        position: center,
        map: map,
        icon: icon
    });
}

$('#novosib-map').on('click touchdown', function (e) {
    e.preventDefault();
    map.setCenter({lat: 55.025638, lng: 82.930100});
    marker.serPosition({lat: 55.025638, lng: 82.930100})
});
$('#sanck-map').on('click touchdown', function (e) {
    e.preventDefault();
    map.setCenter({lat: 59.936788, lng: 30.403661});
    marker.serPosition({lat: 59.936788, lng: 30.403661})
});

